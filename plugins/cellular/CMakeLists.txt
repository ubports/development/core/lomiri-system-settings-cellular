set(CMAKE_AUTOMOC ON)

add_subdirectory(Components)
add_subdirectory(plugin)

set(QML_SOURCES
    apn_manager.js
    apn_editor.js
    carriers.js
    PageApnEditor.qml
    PageChooseApn.qml
    PageChooseCarrier.qml
    PageCarrierAndApn.qml
    PageCarriersAndApns.qml
    PageComponent.qml
    sims.js
    i18nd.js
)

add_library(LomiriCellularPanel MODULE
    plugin.cpp
    plugin.h
    cellular.cpp
    cellular.h
    connectivity.cpp
    connectivity.h
    nm_manager_proxy.h
    nm_settings_proxy.h
    nm_settings_connection_proxy.h
    ${QML_SOURCES}
)
target_link_libraries(LomiriCellularPanel
    LomiriSystemSettingsPrivate
    Qt5::Qml
    Qt5::Quick
    Qt5::DBus)

if(ENABLE_LIBDEVICEINFO)
    add_definitions(-DENABLE_DEVICEINFO)
    target_link_libraries(LomiriCellularPanel PkgConfig::DeviceInfo)
endif()

set(PLUG_DIR ${PLUGIN_PRIVATE_MODULE_DIR}/Lomiri/SystemSettings/Cellular)
install(TARGETS LomiriCellularPanel DESTINATION ${PLUG_DIR})
install(FILES qmldir DESTINATION ${PLUG_DIR})
install(FILES ${QML_SOURCES} DESTINATION ${PLUGIN_QML_DIR}/cellular)
install(FILES cellular.settings DESTINATION ${PLUGIN_MANIFEST_DIR})
install(FILES settings-cellular.svg DESTINATION ${PLUGIN_MANIFEST_DIR}/icons)
