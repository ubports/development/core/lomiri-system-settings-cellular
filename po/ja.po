# Japanese translation for lomiri-system-settings-cellular
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-cellular package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-cellular\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-10-08 20:06+0000\n"
"PO-Revision-Date: 2024-10-13 11:54+0000\n"
"Last-Translator: umesaburo sagawa <atowa-notonare-yamatonare427@pm.me>\n"
"Language-Team: Japanese <https://hosted.weblate.org/projects/lomiri/lomiri-"
"system-settings-cellular/ja/>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.8-dev\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"

#: ../plugins/cellular/Components/DataMultiSim.qml:37
msgid "Cellular data"
msgstr "モバイルデータ"

#: ../plugins/cellular/Components/DataMultiSim.qml:74
#: ../plugins/cellular/Components/NoSim.qml:37
msgid "No SIM detected"
msgstr "SIM が検出されませんでした"

#: ../plugins/cellular/Components/DataMultiSim.qml:82
#: ../plugins/cellular/Components/NoSim.qml:46
msgid "Insert a SIM, then restart the device."
msgstr "SIM を挿入し、デバイスを再起動します。"

#: ../plugins/cellular/Components/DataMultiSim.qml:148
#: ../plugins/cellular/Components/SingleSim.qml:93
msgid "Data roaming"
msgstr "データローミング"

#: ../plugins/cellular/Components/DefaultSim.qml:34
msgid "Ask me each time"
msgstr "毎回尋ねる"

#: ../plugins/cellular/Components/DefaultSim.qml:41
msgid "For outgoing calls, use:"
msgstr "発信するには:"

#: ../plugins/cellular/Components/DefaultSim.qml:58
msgid ""
"You can change the SIM for individual calls, or for contacts in the address "
"book."
msgstr "個別の通話またはアドレス帳の連絡先ごとに SIM を変更できます。"

#: ../plugins/cellular/Components/DefaultSim.qml:63
msgid "For messages, use:"
msgstr "メッセージを送るには:"

#. TRANSLATORS: This is the text that will be used on the "return" key for the virtual keyboard,
#. this word must be less than 5 characters
#: ../plugins/cellular/Components/LabelTextField.qml:52
msgid "Next"
msgstr "次"

#: ../plugins/cellular/Components/MultiSim.qml:56
#: ../plugins/cellular/Components/SingleSim.qml:106
msgid "Data usage statistics"
msgstr "データ利用統計"

#: ../plugins/cellular/Components/MultiSim.qml:61
msgid "Carriers"
msgstr "キャリア"

#: ../plugins/cellular/Components/MultiSim.qml:84
#: ../plugins/cellular/Components/RadioSingleSim.qml:32
msgid "Connection type:"
msgstr "接続タイプ:"

#: ../plugins/cellular/Components/Sim.qml:48
msgid "2G only (saves battery)"
msgstr "2Gのみ (バッテリー消費小)"

#: ../plugins/cellular/Components/Sim.qml:49
msgid "2G/3G"
msgstr "2G/3G"

#: ../plugins/cellular/Components/Sim.qml:50
msgid "2G/3G/4G"
msgstr "2G/3G/4G (高速)"

#: ../plugins/cellular/Components/Sim.qml:51
msgid "2G/3G/4G/5G"
msgstr "2G/3G/4G/5G"

#: ../plugins/cellular/Components/SimEditor.qml:89
msgid "Edit SIM Name"
msgstr "SIM名を編集"

#: ../plugins/cellular/Components/SimEditor.qml:204
#: ../plugins/cellular/PageChooseApn.qml:193
#: ../plugins/cellular/PageChooseApn.qml:224
#: ../plugins/cellular/PageChooseApn.qml:255
#: ../plugins/cellular/PageChooseApn.qml:289
msgid "Cancel"
msgstr "キャンセル"

#: ../plugins/cellular/Components/SimEditor.qml:215
msgid "OK"
msgstr "OK"

#: ../plugins/cellular/Components/SingleSim.qml:70
msgid "Cellular data:"
msgstr "携帯電話データ:"

#: ../plugins/cellular/Components/SingleSim.qml:111
#: ../plugins/cellular/PageCarrierAndApn.qml:30
msgid "Carrier & APN"
msgstr "キャリアとAPN"

#: ../plugins/cellular/PageApnEditor.qml:72
msgid "Edit"
msgstr "編集"

#: ../plugins/cellular/PageApnEditor.qml:72
msgid "New APN"
msgstr "新規APN"

#: ../plugins/cellular/PageApnEditor.qml:133
msgid "Used for:"
msgstr "に使用される："

#: ../plugins/cellular/PageApnEditor.qml:137
#: ../plugins/cellular/PageChooseApn.qml:122
msgid "Internet and MMS"
msgstr "インターネットとMMS"

#: ../plugins/cellular/PageApnEditor.qml:138
#: ../plugins/cellular/PageChooseApn.qml:124
msgid "Internet"
msgstr "インターネット"

#: ../plugins/cellular/PageApnEditor.qml:139
#: ../plugins/cellular/PageChooseApn.qml:128
msgid "MMS"
msgstr "MMS"

#: ../plugins/cellular/PageApnEditor.qml:140
#: ../plugins/cellular/PageChooseApn.qml:126
msgid "LTE"
msgstr "LTE"

#: ../plugins/cellular/PageApnEditor.qml:158
msgid "Name"
msgstr "名前"

#: ../plugins/cellular/PageApnEditor.qml:167
msgid "Enter a name describing the APN"
msgstr "APNの名称を入力"

#. TRANSLATORS: This string is a description of a text
#. field and should thus be concise.
#: ../plugins/cellular/PageApnEditor.qml:183
#: ../plugins/cellular/PageCarrierAndApn.qml:62
#: ../plugins/cellular/PageCarriersAndApns.qml:74
#: ../plugins/cellular/PageChooseApn.qml:43
msgid "APN"
msgstr "APN"

#: ../plugins/cellular/PageApnEditor.qml:193
msgid "Enter the name of the access point"
msgstr "アクセスポイントの名称を入力"

#: ../plugins/cellular/PageApnEditor.qml:207
msgid "MMSC"
msgstr "MMSC"

#: ../plugins/cellular/PageApnEditor.qml:217
msgid "Enter message center"
msgstr "メッセージセンターを入力"

#: ../plugins/cellular/PageApnEditor.qml:235
msgid "Proxy"
msgstr "プロキシ"

#: ../plugins/cellular/PageApnEditor.qml:245
msgid "Enter message proxy"
msgstr "メッセージプロキシを入力"

#: ../plugins/cellular/PageApnEditor.qml:290
msgid "Proxy port"
msgstr "プロキシポート"

#: ../plugins/cellular/PageApnEditor.qml:301
msgid "Enter message proxy port"
msgstr "メッセージプロキシポートを入力"

#: ../plugins/cellular/PageApnEditor.qml:319
msgid "User name"
msgstr "ユーザ名"

#: ../plugins/cellular/PageApnEditor.qml:329
msgid "Enter username"
msgstr "ユーザ名を入力"

#: ../plugins/cellular/PageApnEditor.qml:342
msgid "Password"
msgstr "パスワード"

#: ../plugins/cellular/PageApnEditor.qml:354
msgid "Enter password"
msgstr "パスワードを入力"

#: ../plugins/cellular/PageApnEditor.qml:368
msgid "Authentication"
msgstr "認証"

#: ../plugins/cellular/PageApnEditor.qml:372
#: ../plugins/cellular/PageCarrierAndApn.qml:50
#: ../plugins/cellular/PageCarriersAndApns.qml:62
msgid "None"
msgstr "なし"

#: ../plugins/cellular/PageApnEditor.qml:373
msgid "PAP or CHAP"
msgstr "PAPまたはCHAP"

#: ../plugins/cellular/PageApnEditor.qml:374
msgid "PAP only"
msgstr "PAPのみ"

#: ../plugins/cellular/PageApnEditor.qml:375
msgid "CHAP only"
msgstr "CHAPのみ"

#: ../plugins/cellular/PageApnEditor.qml:386
msgid "Protocol"
msgstr "プロトコル"

#: ../plugins/cellular/PageApnEditor.qml:390
msgid "IPv4"
msgstr "IPv4"

#: ../plugins/cellular/PageApnEditor.qml:391
msgid "IPv6"
msgstr "IPv6"

#: ../plugins/cellular/PageApnEditor.qml:392
msgid "IPv4v6"
msgstr "IPv4v6"

#: ../plugins/cellular/PageCarrierAndApn.qml:48
#: ../plugins/cellular/PageCarrierAndApn.qml:57
#: ../plugins/cellular/PageCarriersAndApns.qml:59
#: ../plugins/cellular/PageChooseCarrier.qml:33
#: ../plugins/cellular/PageChooseCarrier.qml:130
msgid "Carrier"
msgstr "キャリア"

#: ../plugins/cellular/PageCarrierAndApn.qml:72
#: ../plugins/cellular/PageCarriersAndApns.qml:83
msgid "4G calling (VoLTE)"
msgstr "4G通話（VoLTE)"

#: ../plugins/cellular/PageCarriersAndApns.qml:30
msgid "Carriers & APNs"
msgstr "キャリアとAPN"

#: ../plugins/cellular/PageChooseApn.qml:100
msgid "MMS APN"
msgstr "MMS APN"

#: ../plugins/cellular/PageChooseApn.qml:106
msgid "Internet APN"
msgstr "インターネットAPN"

#: ../plugins/cellular/PageChooseApn.qml:112
msgid "LTE APN"
msgstr "LTE APN"

#: ../plugins/cellular/PageChooseApn.qml:171
msgid "Reset All APN Settings…"
msgstr "APN設定を全てリセット…"

#: ../plugins/cellular/PageChooseApn.qml:180
msgid "Reset APN Settings"
msgstr "APN設定をリセット"

#: ../plugins/cellular/PageChooseApn.qml:181
msgid "Are you sure that you want to Reset APN Settings?"
msgstr "APN設定をリセットしてもよいですか？"

#: ../plugins/cellular/PageChooseApn.qml:184
msgid "Reset"
msgstr "リセット"

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”.
#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”.
#: ../plugins/cellular/PageChooseApn.qml:207
#: ../plugins/cellular/PageChooseApn.qml:238
#, qt-format
msgid "Prefer %1"
msgstr "%1を優先"

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”, i.e. used to retrieve MMS messages. %2 is the Internet
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:211
#, qt-format
msgid "You have chosen %1 as your preferred MMS APN. "
msgstr "MMS APNに%1を優先するよう選択しました。 "

#: ../plugins/cellular/PageChooseApn.qml:216
#: ../plugins/cellular/PageChooseApn.qml:281
msgid "Disconnect"
msgstr "切断する"

#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”, i.e. used to connect to the Internet. %2 is the MMS
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:242
#, qt-format
msgid "You have chosen %1 as your preferred Internet APN. "
msgstr "インターネットAPNに%1を優先するよう選択しました。 "

#: ../plugins/cellular/PageChooseApn.qml:247
#: ../plugins/cellular/PageChooseApn.qml:280
msgid "Disable"
msgstr "無効化"

#: ../plugins/cellular/PageChooseApn.qml:269
#, qt-format
msgid "Disconnect %1"
msgstr "%1から切断する"

#: ../plugins/cellular/PageChooseApn.qml:270
#, qt-format
msgid "Disable %1"
msgstr "%1を無効化する"

#: ../plugins/cellular/PageChooseApn.qml:275
#, qt-format
msgid "This disconnects %1."
msgstr "%1から切断します。"

#: ../plugins/cellular/PageChooseApn.qml:276
#, qt-format
msgid "This disables %1."
msgstr "%1を無効化します。"

#: ../plugins/cellular/PageChooseApn.qml:318
msgid "Delete"
msgstr "削除"

#: ../plugins/cellular/PageChooseCarrier.qml:154
msgid "Automatically"
msgstr "自動"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: ../plugins/cellular/PageComponent.qml:33 plugin-strings.generated.js:2
msgid "Cellular"
msgstr "セルラー"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:4
msgid "cellular"
msgstr "電話"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:6
msgid "network"
msgstr "ネットワーク"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:8
msgid "mobile"
msgstr "モバイル"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:10
msgid "gsm"
msgstr "gsm"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:12
msgid "data"
msgstr "データ"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:14
msgid "carrier"
msgstr "キャリア"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:16
msgid "5g"
msgstr "5G"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:18
msgid "4g"
msgstr "4G"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:20
msgid "3g"
msgstr "3G"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:22
msgid "2g"
msgstr "2G"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:24
msgid "lte"
msgstr "削除"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:26
msgid "nr"
msgstr "nr"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:28
msgid "apn"
msgstr "apn"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:30
msgid "roam"
msgstr "ローム"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:32
msgid "sim"
msgstr "sim"

#~ msgid "2G/3G (faster)"
#~ msgstr "2G/3G (高速)"
