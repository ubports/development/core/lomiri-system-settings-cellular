# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the lomiri-system-settings-cellular package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-cellular\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-10-08 20:06+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: as\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../plugins/cellular/Components/DataMultiSim.qml:37
msgid "Cellular data"
msgstr ""

#: ../plugins/cellular/Components/DataMultiSim.qml:74
#: ../plugins/cellular/Components/NoSim.qml:37
msgid "No SIM detected"
msgstr ""

#: ../plugins/cellular/Components/DataMultiSim.qml:82
#: ../plugins/cellular/Components/NoSim.qml:46
msgid "Insert a SIM, then restart the device."
msgstr ""

#: ../plugins/cellular/Components/DataMultiSim.qml:148
#: ../plugins/cellular/Components/SingleSim.qml:93
msgid "Data roaming"
msgstr ""

#: ../plugins/cellular/Components/DefaultSim.qml:34
msgid "Ask me each time"
msgstr ""

#: ../plugins/cellular/Components/DefaultSim.qml:41
msgid "For outgoing calls, use:"
msgstr ""

#: ../plugins/cellular/Components/DefaultSim.qml:58
msgid ""
"You can change the SIM for individual calls, or for contacts in the address "
"book."
msgstr ""

#: ../plugins/cellular/Components/DefaultSim.qml:63
msgid "For messages, use:"
msgstr ""

#. TRANSLATORS: This is the text that will be used on the "return" key for the virtual keyboard,
#. this word must be less than 5 characters
#: ../plugins/cellular/Components/LabelTextField.qml:52
msgid "Next"
msgstr ""

#: ../plugins/cellular/Components/MultiSim.qml:56
#: ../plugins/cellular/Components/SingleSim.qml:106
msgid "Data usage statistics"
msgstr ""

#: ../plugins/cellular/Components/MultiSim.qml:61
msgid "Carriers"
msgstr ""

#: ../plugins/cellular/Components/MultiSim.qml:84
#: ../plugins/cellular/Components/RadioSingleSim.qml:32
msgid "Connection type:"
msgstr ""

#: ../plugins/cellular/Components/Sim.qml:48
msgid "2G only (saves battery)"
msgstr ""

#: ../plugins/cellular/Components/Sim.qml:49
msgid "2G/3G"
msgstr ""

#: ../plugins/cellular/Components/Sim.qml:50
msgid "2G/3G/4G"
msgstr ""

#: ../plugins/cellular/Components/Sim.qml:51
msgid "2G/3G/4G/5G"
msgstr ""

#: ../plugins/cellular/Components/SimEditor.qml:89
msgid "Edit SIM Name"
msgstr ""

#: ../plugins/cellular/Components/SimEditor.qml:204
#: ../plugins/cellular/PageChooseApn.qml:193
#: ../plugins/cellular/PageChooseApn.qml:224
#: ../plugins/cellular/PageChooseApn.qml:255
#: ../plugins/cellular/PageChooseApn.qml:289
msgid "Cancel"
msgstr ""

#: ../plugins/cellular/Components/SimEditor.qml:215
msgid "OK"
msgstr ""

#: ../plugins/cellular/Components/SingleSim.qml:70
msgid "Cellular data:"
msgstr ""

#: ../plugins/cellular/Components/SingleSim.qml:111
#: ../plugins/cellular/PageCarrierAndApn.qml:30
msgid "Carrier & APN"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:72
msgid "Edit"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:72
msgid "New APN"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:133
msgid "Used for:"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:137
#: ../plugins/cellular/PageChooseApn.qml:122
msgid "Internet and MMS"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:138
#: ../plugins/cellular/PageChooseApn.qml:124
msgid "Internet"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:139
#: ../plugins/cellular/PageChooseApn.qml:128
msgid "MMS"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:140
#: ../plugins/cellular/PageChooseApn.qml:126
msgid "LTE"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:158
msgid "Name"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:167
msgid "Enter a name describing the APN"
msgstr ""

#. TRANSLATORS: This string is a description of a text
#. field and should thus be concise.
#: ../plugins/cellular/PageApnEditor.qml:183
#: ../plugins/cellular/PageCarrierAndApn.qml:62
#: ../plugins/cellular/PageCarriersAndApns.qml:74
#: ../plugins/cellular/PageChooseApn.qml:43
msgid "APN"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:193
msgid "Enter the name of the access point"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:207
msgid "MMSC"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:217
msgid "Enter message center"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:235
msgid "Proxy"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:245
msgid "Enter message proxy"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:290
msgid "Proxy port"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:301
msgid "Enter message proxy port"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:319
msgid "User name"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:329
msgid "Enter username"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:342
msgid "Password"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:354
msgid "Enter password"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:368
msgid "Authentication"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:372
#: ../plugins/cellular/PageCarrierAndApn.qml:50
#: ../plugins/cellular/PageCarriersAndApns.qml:62
msgid "None"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:373
msgid "PAP or CHAP"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:374
msgid "PAP only"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:375
msgid "CHAP only"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:386
msgid "Protocol"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:390
msgid "IPv4"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:391
msgid "IPv6"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:392
msgid "IPv4v6"
msgstr ""

#: ../plugins/cellular/PageCarrierAndApn.qml:48
#: ../plugins/cellular/PageCarrierAndApn.qml:57
#: ../plugins/cellular/PageCarriersAndApns.qml:59
#: ../plugins/cellular/PageChooseCarrier.qml:33
#: ../plugins/cellular/PageChooseCarrier.qml:130
msgid "Carrier"
msgstr ""

#: ../plugins/cellular/PageCarrierAndApn.qml:72
#: ../plugins/cellular/PageCarriersAndApns.qml:83
msgid "4G calling (VoLTE)"
msgstr ""

#: ../plugins/cellular/PageCarriersAndApns.qml:30
msgid "Carriers & APNs"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:100
msgid "MMS APN"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:106
msgid "Internet APN"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:112
msgid "LTE APN"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:171
msgid "Reset All APN Settings…"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:180
msgid "Reset APN Settings"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:181
msgid "Are you sure that you want to Reset APN Settings?"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:184
msgid "Reset"
msgstr ""

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”.
#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”.
#: ../plugins/cellular/PageChooseApn.qml:207
#: ../plugins/cellular/PageChooseApn.qml:238
#, qt-format
msgid "Prefer %1"
msgstr ""

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”, i.e. used to retrieve MMS messages. %2 is the Internet
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:211
#, qt-format
msgid "You have chosen %1 as your preferred MMS APN. "
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:216
#: ../plugins/cellular/PageChooseApn.qml:281
msgid "Disconnect"
msgstr ""

#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”, i.e. used to connect to the Internet. %2 is the MMS
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:242
#, qt-format
msgid "You have chosen %1 as your preferred Internet APN. "
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:247
#: ../plugins/cellular/PageChooseApn.qml:280
msgid "Disable"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:269
#, qt-format
msgid "Disconnect %1"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:270
#, qt-format
msgid "Disable %1"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:275
#, qt-format
msgid "This disconnects %1."
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:276
#, qt-format
msgid "This disables %1."
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:318
msgid "Delete"
msgstr ""

#: ../plugins/cellular/PageChooseCarrier.qml:154
msgid "Automatically"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: ../plugins/cellular/PageComponent.qml:33 plugin-strings.generated.js:2
msgid "Cellular"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:4
msgid "cellular"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:6
msgid "network"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:8
msgid "mobile"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:10
msgid "gsm"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:12
msgid "data"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:14
msgid "carrier"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:16
msgid "5g"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:18
msgid "4g"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:20
msgid "3g"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:22
msgid "2g"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:24
msgid "lte"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:26
msgid "nr"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:28
msgid "apn"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:30
msgid "roam"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:32
msgid "sim"
msgstr ""
