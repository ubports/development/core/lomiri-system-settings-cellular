# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-cellular\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-10-08 20:06+0000\n"
"PO-Revision-Date: 2021-03-05 06:27+0000\n"
"Last-Translator: dano6 <dano.kutka@gmail.com>\n"
"Language-Team: Slovak <https://translate.ubports.com/projects/ubports/system-"
"settings/sk/>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"

#: ../plugins/cellular/Components/DataMultiSim.qml:37
msgid "Cellular data"
msgstr "Mobilné dáta"

#: ../plugins/cellular/Components/DataMultiSim.qml:74
#: ../plugins/cellular/Components/NoSim.qml:37
msgid "No SIM detected"
msgstr "Nedetekovaná SIM"

#: ../plugins/cellular/Components/DataMultiSim.qml:82
#: ../plugins/cellular/Components/NoSim.qml:46
msgid "Insert a SIM, then restart the device."
msgstr "Vložte kartu SIM a potom reštartujte zariadenie."

#: ../plugins/cellular/Components/DataMultiSim.qml:148
#: ../plugins/cellular/Components/SingleSim.qml:93
msgid "Data roaming"
msgstr "Dátový roaming"

#: ../plugins/cellular/Components/DefaultSim.qml:34
msgid "Ask me each time"
msgstr "Zakaždým sa spýtať"

#: ../plugins/cellular/Components/DefaultSim.qml:41
msgid "For outgoing calls, use:"
msgstr "Pre odchádzajúce hovory použiť:"

#: ../plugins/cellular/Components/DefaultSim.qml:58
msgid ""
"You can change the SIM for individual calls, or for contacts in the address "
"book."
msgstr ""
"Môžete zmeniť SIM kartu pre jednotlivé hovory alebo kontakty v adresári."

#: ../plugins/cellular/Components/DefaultSim.qml:63
msgid "For messages, use:"
msgstr "Pre správy použiť:"

#. TRANSLATORS: This is the text that will be used on the "return" key for the virtual keyboard,
#. this word must be less than 5 characters
#: ../plugins/cellular/Components/LabelTextField.qml:52
msgid "Next"
msgstr "Ďalej"

#: ../plugins/cellular/Components/MultiSim.qml:56
#: ../plugins/cellular/Components/SingleSim.qml:106
msgid "Data usage statistics"
msgstr "Štatistika využitia dát"

#: ../plugins/cellular/Components/MultiSim.qml:61
msgid "Carriers"
msgstr "Operátori"

#: ../plugins/cellular/Components/MultiSim.qml:84
#: ../plugins/cellular/Components/RadioSingleSim.qml:32
msgid "Connection type:"
msgstr "Typ pripojenia:"

#: ../plugins/cellular/Components/Sim.qml:48
msgid "2G only (saves battery)"
msgstr "Iba 2G (šetrí batériu)"

#: ../plugins/cellular/Components/Sim.qml:49
msgid "2G/3G"
msgstr ""

#: ../plugins/cellular/Components/Sim.qml:50
#, fuzzy
#| msgid "2G/3G/4G (faster)"
msgid "2G/3G/4G"
msgstr "2G/3G/4G (rýchlejší)"

#: ../plugins/cellular/Components/Sim.qml:51
#, fuzzy
#| msgid "2G/3G/4G (faster)"
msgid "2G/3G/4G/5G"
msgstr "2G/3G/4G (rýchlejší)"

#: ../plugins/cellular/Components/SimEditor.qml:89
msgid "Edit SIM Name"
msgstr "Upraviť názov SIM"

#: ../plugins/cellular/Components/SimEditor.qml:204
#: ../plugins/cellular/PageChooseApn.qml:193
#: ../plugins/cellular/PageChooseApn.qml:224
#: ../plugins/cellular/PageChooseApn.qml:255
#: ../plugins/cellular/PageChooseApn.qml:289
msgid "Cancel"
msgstr "Zrušiť"

#: ../plugins/cellular/Components/SimEditor.qml:215
msgid "OK"
msgstr "OK"

#: ../plugins/cellular/Components/SingleSim.qml:70
msgid "Cellular data:"
msgstr "Mobilné dáta:"

#: ../plugins/cellular/Components/SingleSim.qml:111
#: ../plugins/cellular/PageCarrierAndApn.qml:30
msgid "Carrier & APN"
msgstr "Operátor a APN"

#: ../plugins/cellular/PageApnEditor.qml:72
msgid "Edit"
msgstr "Upraviť"

#: ../plugins/cellular/PageApnEditor.qml:72
msgid "New APN"
msgstr "Nový APN"

#: ../plugins/cellular/PageApnEditor.qml:133
msgid "Used for:"
msgstr "Použité pre:"

#: ../plugins/cellular/PageApnEditor.qml:137
#: ../plugins/cellular/PageChooseApn.qml:122
msgid "Internet and MMS"
msgstr "Internet a MMS"

#: ../plugins/cellular/PageApnEditor.qml:138
#: ../plugins/cellular/PageChooseApn.qml:124
msgid "Internet"
msgstr "Internet"

#: ../plugins/cellular/PageApnEditor.qml:139
#: ../plugins/cellular/PageChooseApn.qml:128
msgid "MMS"
msgstr "MMS"

#: ../plugins/cellular/PageApnEditor.qml:140
#: ../plugins/cellular/PageChooseApn.qml:126
msgid "LTE"
msgstr "LTE"

#: ../plugins/cellular/PageApnEditor.qml:158
msgid "Name"
msgstr "Názov"

#: ../plugins/cellular/PageApnEditor.qml:167
msgid "Enter a name describing the APN"
msgstr "Zadajte názov popisujúci APN"

#. TRANSLATORS: This string is a description of a text
#. field and should thus be concise.
#: ../plugins/cellular/PageApnEditor.qml:183
#: ../plugins/cellular/PageCarrierAndApn.qml:62
#: ../plugins/cellular/PageCarriersAndApns.qml:74
#: ../plugins/cellular/PageChooseApn.qml:43
msgid "APN"
msgstr "APN"

#: ../plugins/cellular/PageApnEditor.qml:193
msgid "Enter the name of the access point"
msgstr "Zadajte názov prístupového bodu"

#: ../plugins/cellular/PageApnEditor.qml:207
msgid "MMSC"
msgstr "MMSC"

#: ../plugins/cellular/PageApnEditor.qml:217
msgid "Enter message center"
msgstr "Zadajte centrum správ"

#: ../plugins/cellular/PageApnEditor.qml:235
msgid "Proxy"
msgstr "Proxy"

#: ../plugins/cellular/PageApnEditor.qml:245
msgid "Enter message proxy"
msgstr "Zadajte proxy pre správy"

#: ../plugins/cellular/PageApnEditor.qml:290
msgid "Proxy port"
msgstr "Proxy port"

#: ../plugins/cellular/PageApnEditor.qml:301
msgid "Enter message proxy port"
msgstr "Zadajte proxy port pre správy"

#: ../plugins/cellular/PageApnEditor.qml:319
msgid "User name"
msgstr "Užívateľské meno"

#: ../plugins/cellular/PageApnEditor.qml:329
msgid "Enter username"
msgstr "Zadajte užívateľské meno"

#: ../plugins/cellular/PageApnEditor.qml:342
msgid "Password"
msgstr "Heslo"

#: ../plugins/cellular/PageApnEditor.qml:354
msgid "Enter password"
msgstr "Vložte heslo"

#: ../plugins/cellular/PageApnEditor.qml:368
msgid "Authentication"
msgstr "Overenie"

#: ../plugins/cellular/PageApnEditor.qml:372
#: ../plugins/cellular/PageCarrierAndApn.qml:50
#: ../plugins/cellular/PageCarriersAndApns.qml:62
msgid "None"
msgstr "Žiadne"

#: ../plugins/cellular/PageApnEditor.qml:373
msgid "PAP or CHAP"
msgstr "PAP alebo CHAP"

#: ../plugins/cellular/PageApnEditor.qml:374
msgid "PAP only"
msgstr "Iba PAP"

#: ../plugins/cellular/PageApnEditor.qml:375
msgid "CHAP only"
msgstr "Iba CHAP"

#: ../plugins/cellular/PageApnEditor.qml:386
msgid "Protocol"
msgstr "Protokol"

#: ../plugins/cellular/PageApnEditor.qml:390
msgid "IPv4"
msgstr "IPv4"

#: ../plugins/cellular/PageApnEditor.qml:391
msgid "IPv6"
msgstr "IPv6"

#: ../plugins/cellular/PageApnEditor.qml:392
msgid "IPv4v6"
msgstr "IPv4v6"

#: ../plugins/cellular/PageCarrierAndApn.qml:48
#: ../plugins/cellular/PageCarrierAndApn.qml:57
#: ../plugins/cellular/PageCarriersAndApns.qml:59
#: ../plugins/cellular/PageChooseCarrier.qml:33
#: ../plugins/cellular/PageChooseCarrier.qml:130
msgid "Carrier"
msgstr "Operátor"

#: ../plugins/cellular/PageCarrierAndApn.qml:72
#: ../plugins/cellular/PageCarriersAndApns.qml:83
msgid "4G calling (VoLTE)"
msgstr ""

#: ../plugins/cellular/PageCarriersAndApns.qml:30
msgid "Carriers & APNs"
msgstr "Operátori a APN"

#: ../plugins/cellular/PageChooseApn.qml:100
msgid "MMS APN"
msgstr "MMS APN"

#: ../plugins/cellular/PageChooseApn.qml:106
msgid "Internet APN"
msgstr "Internetový APN"

#: ../plugins/cellular/PageChooseApn.qml:112
msgid "LTE APN"
msgstr "LTE APN"

#: ../plugins/cellular/PageChooseApn.qml:171
msgid "Reset All APN Settings…"
msgstr "Obnoviť všetky nastavenia APN…"

#: ../plugins/cellular/PageChooseApn.qml:180
msgid "Reset APN Settings"
msgstr "Obnoviť nastavenia APN"

#: ../plugins/cellular/PageChooseApn.qml:181
msgid "Are you sure that you want to Reset APN Settings?"
msgstr "Naozaj chcete obnoviť nastavenia APN?"

#: ../plugins/cellular/PageChooseApn.qml:184
msgid "Reset"
msgstr "Obnoviť"

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”.
#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”.
#: ../plugins/cellular/PageChooseApn.qml:207
#: ../plugins/cellular/PageChooseApn.qml:238
#, qt-format
msgid "Prefer %1"
msgstr "Uprednostniť %1"

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”, i.e. used to retrieve MMS messages. %2 is the Internet
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:211
#, qt-format
msgid "You have chosen %1 as your preferred MMS APN. "
msgstr "Zvolili ste %1 ako preferovaný MMS APN. "

#: ../plugins/cellular/PageChooseApn.qml:216
#: ../plugins/cellular/PageChooseApn.qml:281
msgid "Disconnect"
msgstr "Odpojiť"

#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”, i.e. used to connect to the Internet. %2 is the MMS
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:242
#, qt-format
msgid "You have chosen %1 as your preferred Internet APN. "
msgstr "Zvolili ste %1 ako svoj preferovaný internetový APN. "

#: ../plugins/cellular/PageChooseApn.qml:247
#: ../plugins/cellular/PageChooseApn.qml:280
msgid "Disable"
msgstr "Vypnúť"

#: ../plugins/cellular/PageChooseApn.qml:269
#, qt-format
msgid "Disconnect %1"
msgstr "Odpojiť %1"

#: ../plugins/cellular/PageChooseApn.qml:270
#, qt-format
msgid "Disable %1"
msgstr "Zakázať %1"

#: ../plugins/cellular/PageChooseApn.qml:275
#, qt-format
msgid "This disconnects %1."
msgstr "Dôjde k odpojeniu %1."

#: ../plugins/cellular/PageChooseApn.qml:276
#, qt-format
msgid "This disables %1."
msgstr "Zakáže sa %1."

#: ../plugins/cellular/PageChooseApn.qml:318
msgid "Delete"
msgstr "Vymazať"

#: ../plugins/cellular/PageChooseCarrier.qml:154
msgid "Automatically"
msgstr "Automaticky"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: ../plugins/cellular/PageComponent.qml:33 plugin-strings.generated.js:2
msgid "Cellular"
msgstr "Mobilné"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:4
msgid "cellular"
msgstr "mobilná"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:6
msgid "network"
msgstr "sieť"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:8
msgid "mobile"
msgstr "mobilná"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:10
msgid "gsm"
msgstr "GSM"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:12
msgid "data"
msgstr "dáta"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:14
msgid "carrier"
msgstr "operátor"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:16
msgid "5g"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:18
msgid "4g"
msgstr "4G"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:20
msgid "3g"
msgstr "3G"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:22
msgid "2g"
msgstr "2G"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:24
msgid "lte"
msgstr "LTE"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:26
msgid "nr"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:28
msgid "apn"
msgstr "APN"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:30
msgid "roam"
msgstr "ROAM"

#. TRANSLATORS: This is a keyword or name for the cellular plugin which is used while searching
#: plugin-strings.generated.js:32
msgid "sim"
msgstr "SIM"

#~ msgid "2G/3G (faster)"
#~ msgstr "2G/3G (rýchlejší)"
